CREATE TABLE geo_location.geoapi (
geo_api_id  serial not null,
url varchar,
maxResquestPerDay integer,
maxResquestPerSecond integer,


CONSTRAINT geo_location.geoapi_pkey PRIMARY KEY (geo_api_id)

);

CREATE TABLE geo_location.request (
geo_location.request_id serial not null,
full_address varchar,
public_place varchar,
district varchar,
city varchar,
state_ varchar,
zip_code varchar,
geo_location.request_format varchar,

CONSTRAINT geo_location.request_pkey PRIMARY KEY (geo_location.request_id)

);


CREATE TABLE geo_location.search (
data_  timestamp,
generated_response boolean,
geo_location.request_id integer,
geo_api_id integer not null,
error_ varchar,

CONSTRAINT geo_location.search_pkey PRIMARY KEY (geo_location.request_id,geo_api_id),
constraint fk_geo_location.search_geo_location.request FOREIGN key (geo_location.request_id) references geo_location.request(geo_location.request_id),
constraint fk_geo_location.search_geo_location FOREIGN key (geo_api_id) references geo_location.geoapi(geo_api_id)
);

CREATE TABLE response (
geolocation_id serial not null,
full_address varchar,
public_place varchar,
number_ integer,
district varchar,
zip_code varchar,
city varchar,
state_ varchar,
country varchar,
accuracy integer,
latitude float,
longitude float,
place_type varchar,
longWestBBox float,
latNorth_BBox float,
longEast_BBox float,
latSoul_BBox float,
geo_location.request_id integer not null,
geo_api_id integer not null,


CONSTRAINT response_pkey PRIMARY KEY (geolocation_id),
constraint fk_response_geo_location.search FOREIGN key (geo_location.request_id,geo_api_id) references geo_location.search(geo_location.request_id,geo_api_id)

);